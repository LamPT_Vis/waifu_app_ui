import {actionType} from "../actions/constant";

const initialState=[];

export default function(state=initialState, action){
    switch(action.type){
        case actionType.GET_ALL:{
            return getAllWaifus(state,action);
        }
    }
    return state;
} 

function getAllWaifus(state,action){
    const {waifus}=action;
    return[...state,...waifus];
}