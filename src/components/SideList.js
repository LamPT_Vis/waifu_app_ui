import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

class SideList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waifusName: [],
    };
  }
  static propTypes = {
    waifus: PropTypes.array,
  };

  static defaultProps = {
    waifus: [],
  };

  // onClick=()=>{
  //   window.location.reload();
  // }

  render() {
    return (
      <div className="panel panel-primary">
        <div className="panel-heading">
          <h3 className="panel-title">List waifu</h3>
        </div>
        <div className="panel-body">
          <ul className="list-group">
            {
            this.props.waifus.map((waifu, index) => {
              console.log(waifu.name)
              return (
                <NavLink className="list-group-item" key={index} to={`/name/${waifu.name}`} 
                 exact
                  >
                  {waifu.name}
                </NavLink>
              );
            })
            }
          </ul>
          
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const waifus = state.waifuReducer;
  return { waifus };
}

export default connect(mapStateToProps)(SideList);

