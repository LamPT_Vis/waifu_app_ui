import React, { Component } from "react";
import Content from "./components/Content";
import Header from "./components/Header";
import SideList from "./components/SideList";
import * as actions from "./actions/waifuActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waifusList: [],
    };
  }

  static propTypes = {
    waifus: PropTypes.array,
  };

  static defaultProps = {
    waifus: [],
  };

  componentDidMount() {
    fetch("http://localhost:8080/waifu/infor_waifu?lang=en")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ waifusList: data });
      })
      .then(() => {
        this.props.dispatch(actions.getAllWaifu(this.state.waifusList));
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <Header />
        <BrowserRouter>
          <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <SideList />
          </div>
          <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <Switch>
              <Route
                exact
                path="/name/:name"
                children={({ match }) => {
                  return <Content match={match} />;
                }}
              />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const waifus = state.waifuReducer;
  return { waifus };
}

export default connect(mapStateToProps)(App);
