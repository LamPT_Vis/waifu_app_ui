import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import rootReducer from './reducer/index';

const logger=createLogger();

export function configureStore(initialState) {
    return createStore(rootReducer, initialState,applyMiddleware(logger));
  }