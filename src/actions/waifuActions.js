import {actionType} from "./constant";

export function getAllWaifu(waifus){
    return{
        type: actionType.GET_ALL,
        waifus
    }
}