import React, { Component } from 'react';
import {combineReducers} from 'redux';
import waifuReducer from "./waifuReducer";

export default combineReducers({
    waifuReducer
})