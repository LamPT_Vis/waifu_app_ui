import React, { Component } from "react";
import { Route } from "react-router-dom";

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waifu:{}
    };
  }

  componentDidMount() {
    var { match } = this.props;
    var apiUrl=`http://localhost:8080/waifu/infor_waifu/name/${match.params.name}?lang=en`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          waifu:data
        });
      })
      .catch(console.log);
  }

  render() { 
    return (
        <div className="panel panel-info">
          <div className="panel-heading">
            <h3 className="panel-title">Here is your waifuuuuuu!</h3>
          </div>
          <div className="panel-body">
            <div className="thumbnail">
              <img src={this.state.waifu.imgSrc} alt="image" />
              <div className="caption">
                <h3>{this.state.waifu.name}</h3>
                <p>Alias: {this.state.waifu.alias}</p>
                <p>Appearance: {this.state.waifu.appearance}</p>
                <p>Characteristic: {this.state.waifu.characteristic}</p>
                <p>
                  <a className="btn btn-primary">Action</a>
                  <a className="btn btn-default">Action</a>
                </p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
export default Content;
